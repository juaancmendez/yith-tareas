<?php
/**
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Admin' ) ) {
	/**
	 * YITH_PT_Admin
	 */
	class YITH_PT_Admin {
		/**
		 * Main Instance
		 *
		 * @var YITH_PT_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PT_Admin Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PT_Admin constructor.
		 */
		private function __construct() {

			add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
			add_action( 'save_post', array( $this, 'save_meta_box' ) );

			// See definition on class-wp-post-list-table.php  manage_{$post_type}_posts_columns; .
			add_filter( 'manage_testimonial_posts_columns', array( $this, 'add_testimonial_post_type_columns' ) );

			// See definition on class-wp-post-list-table.php manage_{$post->post_type}_posts_custom_column .
			add_action( 'manage_testimonial_posts_custom_column', array( $this, 'display_testimonial_post_type_custom_column' ), 10, 2 );

			// Admin menu.

			add_action( 'admin_menu', array( $this, 'create_menu_for_general_options' ) );
			add_action( 'admin_init', array( $this, 'register_settings' ) );

		}


		/**
		 * Setup the meta boxes
		 */
		public function add_meta_boxes() {
			add_meta_box(
				'yith-pt-additional-information',
				__(
					'Additional information',
					'yith-plugin-testimonials'
				),
				array( $this, 'view_meta_boxes' ),
				YITH_PT_Post_Types::$post_type
			);
		}

		/**
		 * Wiev meta boxes
		 *
		 * @param string? $post .
		 */
		public function view_meta_boxes( $post ) {
			yith_pt_get_view( '/metaboxes/plugin-testimonials-info-metabox.php', array( 'post' => $post ) );
		}

		/**
		 * Save meta box values
		 *
		 * @param string? $post_id .
		 */
		public function save_meta_box( $post_id ) {

			if ( YITH_PT_Post_Types::$post_type !== get_post_type( $post_id ) ) {
				return;
			}

			if ( isset( $_POST['_yith_pt_role'] ) ) {
				update_post_meta( $post_id, '_yith_pt_role', $_POST['_yith_pt_role'] );
			}

			if ( isset( $_POST['_yith_pt_company'] ) ) {
				update_post_meta( $post_id, '_yith_pt_company', $_POST['_yith_pt_company'] );
			}

			if ( isset( $_POST['_yith_pt_company_url'] ) ) {
				update_post_meta( $post_id, '_yith_pt_company_url', $_POST['_yith_pt_company_url'] );
			}

			if ( isset( $_POST['_yith_pt_email'] ) ) {
				update_post_meta( $post_id, '_yith_pt_email', $_POST['_yith_pt_email'] );
			}

			if ( isset( $_POST['_yith_pt_rating'] ) ) {
				update_post_meta( $post_id, '_yith_pt_rating', $_POST['_yith_pt_rating'] );
			}

			if ( isset( $_POST['_yith_pt_vip'] ) ) {
				update_post_meta( $post_id, '_yith_pt_vip', $_POST['_yith_pt_vip'] );
			}

			if ( isset( $_POST['_yith_pt_badge'] ) ) {
				update_post_meta( $post_id, '_yith_pt_badge', $_POST['_yith_pt_badge'] );
			}

			if ( isset( $_POST['_yith_pt_badge_text'] ) ) {
				update_post_meta( $post_id, '_yith_pt_badge_text', $_POST['_yith_pt_badge_text'] );
			}

			if ( isset( $_POST['_yith_pt_badge_background_color'] ) ) {
				update_post_meta( $post_id, '_yith_pt_badge_background_color', $_POST['_yith_pt_badge_background_color'] );
			}

		}
		/**
		 * Filters the columns displayed in the Posts list table for plugin myplugin post type.
		 *
		 * @param string[] $post_columns An associative array of column headings.
		 */
		public function add_testimonial_post_type_columns( $post_columns ) {

			$new_columns = apply_filters(
				'yith_pt_testimonials_custom_columns',
				array(
					'role'    => esc_html__( 'Role', 'yith-plugin-testimonials' ),
					'company' => esc_html__( 'Company', 'yith-plugin-testimonials' ),
					'email'   => esc_html__( 'Email', 'yith-plugin-testimonials' ),
					'stars'   => esc_html__( 'Stars', 'yith-plugin-testimonials' ),
					'vip'     => esc_html__( 'VIP', 'yith-plugin-testimonials' ),
				)
			);

			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;
		}
		/**
		 * Fires for each custom column of a specific post type in the Posts list table.
		 *
		 * @param string $column_name The name of the column to display.
		 * @param int    $post_id     The current post ID.
		 * */
		public function display_testimonial_post_type_custom_column( $column_name, $post_id ) {

			switch ( $column_name ) {

				case 'role':
					// Operations for get information for role example.
					echo get_post_meta( $post_id, '_yith_pt_role', true );
					break;
				case 'company':
					// Operations for get information for company example.
					echo '<a href="' . get_post_meta( $post_id, '_yith_pt_company_url', true ) . '" target="_blank">' . get_post_meta( $post_id, '_yith_pt_company', true ) . '</a>';
					break;
				case 'email':
					// Operations for get information for email example.
					echo get_post_meta( $post_id, '_yith_pt_email', true );
					break;
				case 'stars':
					// Operations for get information for stars example.
					echo get_post_meta( $post_id, '_yith_pt_rating', true );
					break;
				case 'vip':
					// Operations for get information for vip example.
					echo get_post_meta( $post_id, '_yith_pt_vip', true );
					break;
				default:
					do_action( 'yith_pt_myplugin_display_custom_column', $column, $post_id );
					break;
			}

		}
		/**
		 *  Create menu for general options
		 */
		public function create_menu_for_general_options() {

			// See the following option https://developer.wordpress.org/reference/functions/add_menu_page/ .
				add_menu_page(
					esc_html__( 'Plugin Testimonials Options', 'yith-plugin-testimonials' ),
					esc_html__( 'Plugin Testimonials Options', 'yith-plugin-testimonials' ),
					'manage_options',
					'plugin_testimonials_options',
					array( $this, 'testimonials_custom_menu_page' ),
					'',
					40
				);

		}
		/**
		 *  Callback custom menu page
		 */
		function testimonials_custom_menu_page() {
			yith_pt_get_view( '/admin/plugin-options-panel.php', array() );
		}

		/**
		 * Add the fields in the shortcode attribute management page
		 */
		public function register_settings() {			
			$page_name    = 'pt-options-page';
			$section_name = 'options_section';

			$setting_fields = array(
				array(
					'id'       => 'yith_pt_shortcode_show_photo',
					'title'    => esc_html__( 'Show photo', 'yith-plugin-testimonials' ),
					'callback' => 'print_show_photo',
				),
				array(
					'id'       => 'yith_pt_shortcode_testimonials_to_show',
					'title'    => esc_html__( 'Testimonials to show', 'yith-plugin-testimonials' ),
					'callback' => 'print_testimonials_to_show',
				),
				array(
					'id'       => 'yith_pt_shortcode_show_testimonials_border_radius',
					'title'    => esc_html__( 'Set border radius', 'yith-plugin-testimonials' ),
					'callback' => 'print_show_testimonials_border_radius',
				),
				array(
					'id'       => 'yith_pt_shortcode_testimonials_links_color',
					'title'    => esc_html__( 'Set testimonials link color', 'yith-plugin-testimonials' ),
					'callback' => 'print_testimonials_links_color',
				),
				array(
					'id'       => 'yith_pt_shortcode_print_testimonials_hover_effect',
					'title'    => esc_html__( 'Set testimonials hover effect', 'yith-plugin-testimonials' ),
					'callback' => 'print_testimonials_hover_effect',
				),
			);

			add_settings_section(
				$section_name,
				esc_html__( 'Section', 'yith-plugin-testimonials' ),
				'',
				$page_name
			);

			foreach ( $setting_fields as $field ) {
				extract( $field );

				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id )
				);

				register_setting( $page_name, $id );
			}

		}

		/**
		 * Print the show testimonials_to_show toggle field
		 */
		public function print_testimonials_to_show() {
			$tst_number = strval( get_option( 'yith_pt_shortcode_testimonials_to_show', 6 ) );
			?>
			<input type="number" id="yith_pt_shortcode_testimonials_to_show" name="yith_pt_shortcode_testimonials_to_show"
			value="<?php echo '' !== $tst_number ? $tst_number : 6; ?>" min="0">
			<?php
		}

		/**
		 * Print the show image toggle field
		 */
		public function print_show_photo() {
			?>
			<input type="checkbox" class="pt-tst-option-panel__onoff__input" name="yith_pt_shortcode_show_photo"
				value='yes'
				id="yith_pt_shortcode_show_photo"
				<?php checked( get_option( 'yith_pt_shortcode_show_photo', '' ), 'yes' ); ?>
			>
			<label for="shortcode_show_photo" class="pt-tst-option-panel__onoff__label ">
				<span class="pt-tst-option-panel__onoff__btn"></span>
			</label>
			<?php
		}

		/**
		 * Print_show_testimonials_border_radius
		 *
		 * @return void
		 */
		public function print_show_testimonials_border_radius() {
			$tst_number = strval( get_option( 'yith_pt_shortcode_show_testimonials_border_radius', 7 ) );
			?>
			<input type="number" id="yith_pt_shortcode_show_testimonials_border_radius" name="yith_pt_shortcode_show_testimonials_border_radius"
			value="<?php echo '' !== $tst_number ? $tst_number : 7; ?>" min="0">
			<?php
		}

		/**
		 * Print_testimonials_links_color
		 *
		 * @return void
		 */
		public function print_testimonials_links_color() {
			$tst_text = strval( get_option( 'yith_pt_shortcode_testimonials_links_color', '#0073aa' ) );
			?>
			<input type="text" id="yith_pt_shortcode_testimonials_links_color" name="yith_pt_shortcode_testimonials_links_color"
			value="<?php echo '' !== $tst_text ? $tst_text : '#0073aa'; ?>">
			<?php
		}

		/**
		 * Print_testimonials_hover_effect
		 *
		 * @return void
		 */
		public function print_testimonials_hover_effect() {

			?>
			<select id="yith_pt_shortcode_print_testimonials_hover_effect" name="yith_pt_shortcode_print_testimonials_hover_effect">
			<option value="none"
			<?php
			if ( 'none' === get_option( 'yith_pt_shortcode_print_testimonials_hover_effect', '' ) ) {
				echo 'selected';
			};
			?>
			>
				None
			</option>
			<option value="zoom" 
			<?php
			if ( 'zoom' === get_option( 'yith_pt_shortcode_print_testimonials_hover_effect', '' ) ) {
				echo 'selected';
			};
			?>
			>
				Zoom
			</option>
			<option value="highlight"
			<?php
			if ( 'highlight' === get_option( 'yith_pt_shortcode_print_testimonials_hover_effect', '' ) ) {
				echo 'selected';
			};
			?>
			>
				Highlight
			</option>
			</select>
			<?php

		}
	}
}
