<?php

?>

<div class="ga-tst-form">
<!-- Additional Information fields -->
    <div class="ga-tst-form__input ga-tst-form__input-role">
        <label class="ga-tst-form__input-role__label" for="tst_role"><?php _e( 'Role:', 'yith-plugin-testimonials' ); ?></label>
        <input type="text" class="ga-tst-form__input-role__input" name="_yith_pt_role" id="tst_role"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_role', true ) ); ?>" placeholder="e.g. 'developer'">
    </div>
    <div class="ga-tst-form__input ga-tst-form__input-company">
        <label class="ga-tst-form__input-company__label" for="tst_company"><?php _e( 'Company:', 'yith-plugin-testimonials' ); ?></label>
        <input type="text" class="ga-tst-form__input-company__input" name="_yith_pt_company" id="tst_company"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_company', true ) ); ?>" placeholder="e.g. 'yith'">
    </div>
    <div class="ga-tst-form__input ga-tst-form__input-company_url">
        <label class="ga-tst-form__input-company_url__label" for="tst_company_url"><?php _e( 'Company-url:', 'yith-plugin-testimonials' ); ?></label>
        <input type="url" class="ga-tst-form__input-company_url__input" name="_yith_pt_company_url" id="tst_company_url"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_company_url', true ) ); ?>" placeholder="e.g. 'https://yithemes.com'">
    </div>
    <div class="ga-tst-form__input ga-tst-form__input-email">
        <label class="ga-tst-form__input-email__label" for="tst_email"><?php _e( 'Email:', 'yith-plugin-testimonials' ); ?></label>
        <input type="mail" class="ga-tst-form__input-email__input" name="_yith_pt_email" id="tst_email"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_email', true ) ); ?>" placeholder="e.g. 'juan@mail.com'">
    </div>
    <div class="ga-tst-form__input ga-tst-form__input-rating">
        <label class="ga-tst-form__input-rating__label" for="tst_rating"><?php _e( 'Rating:', 'yith-plugin-testimonials' ); ?></label>
        <input type="number" class="ga-tst-form__input-rating__input" name="_yith_pt_rating" id="tst_rating" min="1" max="5"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_rating', true ) ); ?>" placeholder="e.g. 1 to 5">
    </div>
    <div class="ga-tst-form__input ga-tst-form__input-vip">
        <label class="ga-tst-form__input-vip__label" for="tst_vip"><?php _e( 'VIP:', 'yith-plugin-testimonials' ); ?></label>
        <input type="text" class="ga-tst-form__input-vip__input" name="_yith_pt_vip" id="tst_vip"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_vip', true ) ); ?>"
                <?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_vip', true ) ) ?> placeholder="e.g. 'yes' or 'no'">
    </div>
    <div class="ga-tst-form__input ga-tst-form__input-badge">
        <label class="ga-tst-form__input-badge__label" for="tst_badge"><?php _e( 'Badge:', 'yith-plugin-testimonials' ); ?></label>
        <input type="text" class="ga-tst-form__input-badge__input" name="_yith_pt_badge" id="tst_badge"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_badge', true ) ); ?>" placeholder="e.g. 'yes' or 'no'">
    </div>
    <div class="ga-tst-form__input ga-tst-form__input-badge_text">
        <label class="ga-tst-form__input-badge_text__label" for="tst_badge_text"><?php _e( 'Badge text:', 'yith-plugin-testimonials' ); ?></label>
        <input type="text" class="ga-tst-form__input-badge_text__input" name="_yith_pt_badge_text" id="tst_badge_text"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_badge_text', true ) ); ?>" placeholder="e.g. 'popular'> ">
    </div>
    <div class="ga-tst-form__input ga-tst-form__input-badge_background_color">
        <label class="ga-tst-form__input-badge_background_color__label" for="tst_badge_background_color"><?php _e( 'Badge background color:', 'yith-plugin-testimonials' ); ?></label>
        <input type="text" class="ga-tst-form__input-badge_background_color__input" name="_yith_pt_badge_background_color" id="tst_badge_background_color"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_badge_background_color', true ) ); ?>" placeholder="e.g. 'red'">
    </div>
</div>