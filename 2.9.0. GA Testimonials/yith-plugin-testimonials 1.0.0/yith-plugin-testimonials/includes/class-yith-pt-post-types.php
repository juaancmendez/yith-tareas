<?php
/**
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Post_Types' ) ) {
	/**
	 * YITH_PT_Post_Types
	 */
	class YITH_PT_Post_Types {

		/**
		 * Main Instance
		 *
		 * @var YITH_PT_Post_Types
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Post type name
		 *
		 * @var YITH_PT_Post_Types
		 * @since 1.0
		 * @access public
		 */
		public static $post_type = 'testimonial';

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PT_Post_Types Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PT_Post_Types constructor.
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'yith_pt_create_post_type_testimonial' ) );
			add_action( 'init', array( $this, 'register_taxonomy' ) );
		}

		/**
		 * Yith_pt_create_post_type_testimonial
		 *
		 * @return void
		 */
		public function yith_pt_create_post_type_testimonial() {

			$args = array(
				'label'        => __( 'Testimonial', 'yith-plugin-testimonials' ),
				'description'  => __( 'Testimonial post type', 'yith-plugin-testimonials' ),
				'public'       => true,
				'menu_icon'    => 'dashicons-universal-access',
				'show_in_menu' => true,
				'show_ui'      => true,
				'rewrite'      => false,
				'supports'     => array( 'title', 'editor', 'author', 'thumbnail' ),
			);
			register_post_type( self::$post_type, $args );

		}

		/**
		 * Register_taxonomy
		 *
		 * @return void
		 */
		public function register_taxonomy() {
			// Add new taxonomy, make it hierarchical (like categories).
			$labels = array(
				'name'              => _x( 'Hierarchical taxonomy', 'taxonomy general name', 'yith-plugin-testimonials' ),
				'singular_name'     => _x( 'Hierarchical', 'taxonomy singular name', 'yith-plugin-testimonials' ),
				'search_items'      => __( 'Search Hierarchical', 'yith-plugin-testimonials' ),
				'all_items'         => __( 'All Hierarchical', 'yith-plugin-testimonials' ),
				'parent_item'       => __( 'Parent Hierarchical', 'yith-plugin-testimonials' ),
				'parent_item_colon' => __( 'Parent Hierarchical:', 'yith-plugin-testimonials' ),
				'edit_item'         => __( 'Edit Hierarchical', 'yith-plugin-testimonials' ),
				'update_item'       => __( 'Update Hierarchical', 'yith-plugin-testimonials' ),
				'add_new_item'      => __( 'Add New Hierarchical', 'yith-plugin-testimonials' ),
				'new_item_name'     => __( 'New Hierarchical Name', 'yith-plugin-testimonials' ),
				'menu_name'         => __( 'Hierarchical', 'yith-plugin-testimonials' ),
			);

			$args = array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'yith_tax_hierarchical' ),
			);

			register_taxonomy( 'yith_pt_hietatchical_tax', array( self::$post_type ), $args );

			// Add new taxonomy, make it no hierarchical (like tags).

			$labels1 = array(
				'name'              => _x( 'No Hierarchical taxonomy', 'taxonomy general name', 'yith-plugin-testimonials' ),
				'singular_name'     => _x( 'No Hierarchical', 'taxonomy singular name', 'yith-plugin-testimonials' ),
				'search_items'      => __( 'Search No Hierarchical', 'yith-plugin-testimonials' ),
				'all_items'         => __( 'All No Hierarchical', 'yith-plugin-testimonials' ),
				'parent_item'       => __( 'Parent No Hierarchical', 'yith-plugin-testimonials' ),
				'parent_item_colon' => __( 'Parent No Hierarchical:', 'yith-plugin-testimonials' ),
				'edit_item'         => __( 'Edit No Hierarchical', 'yith-plugin-testimonials' ),
				'update_item'       => __( 'Update No Hierarchical', 'yith-plugin-testimonials' ),
				'add_new_item'      => __( 'Add New No Hierarchical', 'yith-plugin-testimonials' ),
				'new_item_name'     => __( 'New No Hierarchical Name', 'yith-plugin-testimonials' ),
				'menu_name'         => __( 'No Hierarchical', 'yith-plugin-testimonials' ),
			);

			$args1 = array(
				'hierarchical'      => false,
				'labels'            => $labels1,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'yith_tax_no_hierarchical' ),
			);

			register_taxonomy( 'yith_pt_no_hietatchical_tax', array( self::$post_type ), $args1 );

		}

	}
}
