# Copyright (C) 2021 Juan Coronel Mendez
# This file is distributed under the same license as the YITH Plugin Testimonials plugin.
msgid ""
msgstr ""
"Project-Id-Version: YITH Plugin Testimonials 1.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/yith-plugin-testimonials\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-03-18T17:46:58+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.4.0\n"
"X-Domain: yith-plugin-testimonials\n"

#. Plugin Name of the plugin
msgid "YITH Plugin Testimonials"
msgstr ""

#. Description of the plugin
msgid "Testimonials for YITH Plugins"
msgstr ""

#. Author of the plugin
msgid "Juan Coronel Mendez"
msgstr ""

#. Author URI of the plugin
msgid "https://yithemes.com/"
msgstr ""

#: includes/class-yith-pt-admin.php:69
msgid "Additional information"
msgstr ""

#: includes/class-yith-pt-admin.php:145
msgid "ISBN"
msgstr ""

#: includes/class-yith-pt-admin.php:146
msgid "Price"
msgstr ""

#: includes/class-yith-pt-admin.php:147
msgid "Cover Type"
msgstr ""

#: includes/class-yith-pt-admin.php:148
msgid "Language"
msgstr ""

#: includes/class-yith-pt-admin.php:195
#: includes/class-yith-pt-admin.php:196
msgid "Plugin Testimonials Options"
msgstr ""

#: includes/class-yith-pt-admin.php:222
msgid "Show image"
msgstr ""

#: includes/class-yith-pt-admin.php:227
msgid "testimonials to search"
msgstr ""

#: includes/class-yith-pt-admin.php:234
msgid "Section"
msgstr ""

#: includes/class-yith-pt-post-types.php:85
msgid "Testimonial"
msgstr ""

#: includes/class-yith-pt-post-types.php:86
msgid "Testimonial post type"
msgstr ""

#: includes/class-yith-pt-post-types.php:106
msgctxt "taxonomy general name"
msgid "Hierarchical taxonomy"
msgstr ""

#: includes/class-yith-pt-post-types.php:107
msgctxt "taxonomy singular name"
msgid "Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:108
msgid "Search Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:109
msgid "All Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:110
msgid "Parent Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:111
msgid "Parent Hierarchical:"
msgstr ""

#: includes/class-yith-pt-post-types.php:112
msgid "Edit Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:113
msgid "Update Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:114
msgid "Add New Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:115
msgid "New Hierarchical Name"
msgstr ""

#: includes/class-yith-pt-post-types.php:116
msgid "Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:133
msgctxt "taxonomy general name"
msgid "No Hierarchical taxonomy"
msgstr ""

#: includes/class-yith-pt-post-types.php:134
msgctxt "taxonomy singular name"
msgid "No Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:135
msgid "Search No Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:136
msgid "All No Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:137
msgid "Parent No Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:138
msgid "Parent No Hierarchical:"
msgstr ""

#: includes/class-yith-pt-post-types.php:139
msgid "Edit No Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:140
msgid "Update No Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:141
msgid "Add New No Hierarchical"
msgstr ""

#: includes/class-yith-pt-post-types.php:142
msgid "New No Hierarchical Name"
msgstr ""

#: includes/class-yith-pt-post-types.php:143
msgid "No Hierarchical"
msgstr ""

#: views/admin/plugin-options-panel.php:9
msgid "Settings"
msgstr ""

#: views/metaboxes/plugin-testimonials-info-metabox.php:8
msgid "Role:"
msgstr ""

#: views/metaboxes/plugin-testimonials-info-metabox.php:13
msgid "Company:"
msgstr ""

#: views/metaboxes/plugin-testimonials-info-metabox.php:18
msgid "Company-url:"
msgstr ""

#: views/metaboxes/plugin-testimonials-info-metabox.php:23
msgid "Email:"
msgstr ""

#: views/metaboxes/plugin-testimonials-info-metabox.php:28
msgid "Rating:"
msgstr ""

#: views/metaboxes/plugin-testimonials-info-metabox.php:33
msgid "VIP:"
msgstr ""

#: views/metaboxes/plugin-testimonials-info-metabox.php:38
msgid "Badge:"
msgstr ""

#: views/metaboxes/plugin-testimonials-info-metabox.php:43
msgid "Badge text:"
msgstr ""

#: views/metaboxes/plugin-testimonials-info-metabox.php:48
msgid "Badge background color:"
msgstr ""
