<?php

?>

<div class="ga-tst-form">
<!-- Additional Information fields -->
    <div class="ga-tst-form__input ga-tst-form__input-isbn">
        <label class="ga-tst-form__input-isbn__label" for="tst_isbn"><?php _e( 'isbn', 'yith-plugin-testimonials' ); ?></label>
        <input type="text" class="ga-tst-form__input-isbn__input" name="_yith_pt_isbn" id="tst_isbn"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pmp_isbn', true ) ); ?>">
    </div>
    <div class="ga-tst-form__input ga-tst-form__input-price">
        <label class="ga-tst-form__input-price__label" for="tst_price"><?php _e( 'price', 'yith-plugin-testimonials' ); ?></label>
        <input type="text" class="ga-tst-form__input-price__input" name="_yith_pt_price" id="tst_price"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_price', true ) ); ?>">
    </div>
    <div class="ga-tst-form__input ga-tst-form__input-cover_type">
        <label class="ga-tst-form__input-cover_type__label" for="tst_cover_type"><?php _e( 'cover_type', 'yith-plugin-testimonials' ); ?></label>
        <input type="text" class="ga-tst-form__input-cover_type__input" name="_yith_pt_cover_type" id="tst_cover_type"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_cover_type', true ) ); ?>">
    </div>
    <div class="ga-tst-form__input ga-tst-form__input-language">
        <label class="ga-tst-form__input-language__label" for="tst_language"><?php _e( 'language', 'yith-plugin-testimonials' ); ?></label>
        <input type="text" class="ga-tst-form__input-language__input" name="_yith_pt_language" id="tst_language"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_language', true ) ); ?>">
    </div>
</div>