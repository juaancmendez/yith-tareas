<?php
/**
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

?>

<div class="yith-pt-post-container 
	<?php
	if ( 'zoom' === $hover_effect ) {
		echo 'zoom';
	} else {
		if ( 'highlight' === $hover_effect ) {
			echo 'highlight';
		} else {
			echo '';
		}
	}
	?>
	">
	<?php

	if ( 'yes' === $show_photo || empty( $show_photo ) ) {
		?>
		<div class="yith-pt-post-container-photo">
		
		<?php

			echo get_the_post_thumbnail( $post->ID, 'thumbnail', array( 'class' => 'aligncenter' ) );

		?>
		</div>
		<?php
	}

	?>

	<div class="yith-pt-post-title">
	<p>
	<?php echo $post->post_title; ?>
	</p>
	</div>

	

	<div class="yith-pt-post-content">
		<p>
	<?php echo $post->post_content; ?>
</p>
	</div>

</div>