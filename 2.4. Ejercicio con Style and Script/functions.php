<?php

function enqueue_scripts() {
	$theme_version = wp_get_theme()->get( 'Version' );
	wp_enqueue_style( 'mi', get_template_directory_uri() . '/assets/css/mi.css', null, $theme_version );
}

add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );

?>