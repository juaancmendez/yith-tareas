<?php
/**
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Ajax' ) ) {
	/**
	 * YITH_PT_Ajax
	 */
	class YITH_PT_Ajax {
		/**
		 * Main Instance
		 *
		 * @var YITH_PT_Ajax
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PT_Ajax Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PT_Ajax constructor.
		 */
		private function __construct() {

			add_action( 'wp_ajax_testimonials_notify_button_click', array( $this, 'testimonials_notify_button_click' ) );
			add_action( 'wp_ajax_nopriv_testimonials_notify_button_click', array( $this, 'testimonials_notify_button_click' ) );

			add_action( 'wp_ajax_testimonials_vote_action', array( $this, 'testimonials_vote_action' ) );
			add_action( 'wp_ajax_nopriv_testimonials_vote_action', array( $this, 'testimonials_vote_action' ) );

		}
		/**
		 * Función que procesa la llamada AJAX
		 *
		 * @return void
		 */
		public function testimonials_notify_button_click() {

			// Check parameters
			error_log( 'entre a la funcion' );
			$message = isset( $_POST['message'] ) ? $_POST['message'] : 'false';
			if ( ! $message ) {

				wp_send_json( array( 'message' => __( 'Message not received :(', 'wpduf' ) ) );

			} else {

				wp_send_json( array( 'message' => __( 'Message received, greetings from server!', 'wpduf' ) ) );

			}

		}
		/**
		 * Testimonials_vote_up
		 *
		 * @return void
		 */
		public function testimonials_vote_action() {

			$id_post = isset( $_POST['id'] ) ? $_POST['id'] : 'false';
			$id_user = get_current_user_id();
			$message = isset( $_POST['message'] ) ? $_POST['message'] : 'false';
			error_log( 'pase por votitos' );

			if ( 0 === $id_user ) {

				wp_send_json( array( 'user_no_login' => __( 'Only the users login can vote :(', 'wpduf' ) ) );

			} else {

				$vote_users = get_post_meta( $id_post, 'yith_pt_vote_users', true );

				//$prueba = in_array( $id_user, $vote_users, false );
				//error_log($prueba);

				if ( ! in_array( $id_user, $vote_users, false ) ) {

					$vote_users[] = $id_user;
					update_post_meta( $id_post, 'yith_pt_vote_users', $vote_users );
					$value = get_post_meta( $id_post, 'yith_pt_vote_counter', true );

					if ( 'vote-up' === $message ) {

						$value++;

					} elseif ( 'vote-down' === $message ) {

						$value--;

					}

					update_post_meta( $id_post, 'yith_pt_vote_counter', $value );
					wp_send_json(
						array(
							'id'          => __( 'ID, greetings from server!', 'wpduf' ),
							'vote_number' => get_post_meta( $id_post, 'yith_pt_vote_counter', true ),
						),
					);

				} else {
					wp_send_json(
						array(
							'user_cant_vote' => __( 'The user cant vote because ya voto!', 'wpduf' ),
						),
					);
				}
			}

		}

	}
}
