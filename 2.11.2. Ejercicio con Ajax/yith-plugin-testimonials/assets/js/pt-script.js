jQuery(document).ready( function(){
    // Ante cualquier voto, muestra un mensaje "Usted ha votado!"
    jQuery('#data-vote-up,#data-vote-down').on('click', function(){  
       //La llamada AJAX
        jQuery.ajax({
            type : 'post',
            url : wp_ajax_tets_vars.ajax_url,
            data : {
               action: 'testimonials_notify_button_click', 
               message: "Button has been clicked!"
            },
            error: function(response){
               console.log(response)
               jQuery('#txtVote').text('voto error')
            },
            success: function(response) {
               // Actualiza el mensaje con la respuesta
               jQuery('#txtVote').text('Usted ha votado!')
            }
        })
    });

    // Si vota positivo, +1
    jQuery('#data-vote-up').on('click', function(){  
        //La llamada AJAX
        jQuery.ajax({
            type : 'post',
            url : wp_ajax_tets_vars.ajax_url,
            data : {
                id: jQuery(this).attr('data-id'),
                action: 'testimonials_vote_action', 
                message: "vote-up"
            },
            error: function(response){
                console.log(response)
                jQuery('#txtVoteCounter').text('vote-up error')
            },
            success: function(response) {
                // Actualiza el mensaje con la respuesta
                console.log(response);
                if (! response['user_cant_vote'] ) {
                    jQuery('#txtVoteCounter').text('Suma de votos: ' + response['vote_number']);
                } else {
                    jQuery('#txtVote').text('No puede votar dos veces!'); 
                }
            }
        })
    });

    // Si vota negativo, -1
    jQuery('#data-vote-down').on('click', function(){  
        //La llamada AJAX
        jQuery.ajax({
            type : 'post',
            url : wp_ajax_tets_vars.ajax_url,
            data : {
                id: jQuery(this).attr('data-id'),
                action: 'testimonials_vote_action', 
                message: "vote-down"
            },
            error: function(response){
                console.log(response)
                jQuery('#txtVoteCounter').text('vote-down error')
            },
            success: function(response) {
                // Actualiza el mensaje con la respuesta
                console.log(response);
                if (! response['user_cant_vote'] ) {
                    jQuery('#txtVoteCounter').text('Suma de votos: ' + response['vote_number']);
                } else {
                    jQuery('#txtVote').text('No puede votar dos veces!'); 
                }
            }
        })
    });

});