<?php
/**
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Shortcodes' ) ) {
	/**
	 * YITH_PT_Shortcodes
	 */
	class YITH_PT_Shortcodes {

		/**
		 * Main Instance
		 *
		 * @var YITH_PT_Shortcodes
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PT_Post_Types Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PT_Post_Types constructor.
		 */
		private function __construct() {

			$shortcodes = array(
				'yith_pt_show_post_types'                => __CLASS__ . '::show_post_types', // print post.
				'yith_pt_show_testimonials'              => __CLASS__ . '::show_testimonials',  // print testimonial posts.
				'yith_pt_show_latest_three_testimonials' => __CLASS__ . '::show_three_latest_testimonials', // print latest three testimonials.
			);
			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}

		}

		/**
		 * Show_post_types
		 *
		 * @param  args $args .
		 * @return string .
		 */
		public static function show_post_types( $args ) {

			// Enqueue custom CSS for the shortcode.
			wp_enqueue_style( 'yith-pt-frontend-shortcode-css' );

			$args = shortcode_atts(
				array(
					'number_books' => get_option( 'yith_pt_shortcode_show_books_to_search' ),
					'show_image'   => get_option( 'yith_pt_shortcode_show_image' ),
					'post_type'    => 'testimonial',
				),
				$args,
				'yith_auction_products',
			);

			$posts = get_posts( $args );

			ob_start();

			foreach ( $posts as $post ) {
				yith_pt_get_template(
					'/frontend/show_post.php',
					array(
						'post'                 => $post,
						'show_books_to_search' => $args['number_books'],
					)
				);
			}

			return '<div class="yith-pt-posts">' . ob_get_clean() . '</div>';

		}

		/**
		 * Show_testimonials
		 *
		 * @param atts $atts .
		 * @return string $result
		 */
		public function show_testimonials( $atts ) {

			wp_enqueue_style( 'yith-pt-frontend-shortcode-css' );
			wp_enqueue_style( 'yith-pt-frontend-fontawesome-all' );
			wp_enqueue_style( 'yith-pt-frontend-fontawesome' );

			$default_values = array(
				'numberposts'  => get_option( 'yith_pt_shortcode_testimonials_to_show', '6' ),
				'show_photo'   => get_option( 'yith_pt_shortcode_show_photo', 'yes' ),
				'hover_effect' => get_option( 'yith_pt_shortcode_print_testimonials_hover_effect', 'zoom' ),
			);

			$atts = shortcode_atts(
				array(
					'number'       => $atts['number'],
					'ids'          => $atts['ids'],
					'show_photo'   => $atts['show_photo'],
					'hover_effect' => $atts['hover_effect'],
					'category'     => $atts['category'],
				),
				$atts,
			);

			if ( ! empty( $atts['number'] ) ) {
				$default_values['numberposts'] = $atts['number'];
			}

			if ( ! empty( $atts['ids'] ) ) {
				$array_ids = explode( ',', $atts['ids'] );
			}

			$posts = array(
				'numberposts'    => $default_values['numberposts'],
				'include'        => $array_ids,
				'post_type'      => 'testimonial',
				'publish_status' => 'published',
				'orderby'        => 'date',
				'order'          => 'DESC',
				'tax_query'		 => array(
					array(
						'taxonomy' => 'yith_pt_hietatchical_tax',
						'field'    => 'slug',
						'terms'    => $atts['category'],
					),
				),
			);

			if ( empty( $atts['ids'] ) ) {
				unset( $posts['include'] );
			}

			if ( empty( $atts['category'] ) ) {
				unset( $posts['tax_query'] );
			}

			$my_posts = get_posts( $posts );

			if ( ! empty( $my_posts ) ) :

				foreach ( $my_posts as $p ) {

					ob_start();

					yith_pt_get_template(
						'/frontend/show_post.php',
						array(
							'post'                   => $p,
							'show_photo'             => get_option( 'yith_pt_shortcode_show_photo' ),
							'hover_effect'           => get_option( 'yith_pt_shortcode_print_testimonials_hover_effect' ),
							'role'                   => get_post_meta( $p->ID, '_yith_pt_role', true ),
							'company'                => get_post_meta( $p->ID, '_yith_pt_company', true ),
							'company_url'            => get_post_meta( $p->ID, '_yith_pt_company_url', true ),
							'email'                  => get_post_meta( $p->ID, '_yith_pt_email', true ),
							'rating'                 => get_post_meta( $p->ID, '_yith_pt_rating', true ),
							'vip'                    => get_post_meta( $p->ID, '_yith_pt_vip', true ),
							'badge'                  => get_post_meta( $p->ID, '_yith_pt_badge', true ),
							'badge_text'             => get_post_meta( $p->ID, '_yith_pt_badge_text', true ),
							'badge_background_color' => get_post_meta( $p->ID, '_yith_pt_badge_background_color', true ),
							'links_color'            => get_option( 'yith_pt_shortcode_testimonials_links_color' ),
							'border_radius'          => get_option( 'yith_pt_shortcode_show_testimonials_border_radius' ),
						)
					);
				};

				return ob_get_clean();

			endif;

		}

		/**
		 * Show_three_latest_posts
		 *
		 * @return object .
		 */
		public function show_three_latest_testimonials() {

			wp_enqueue_style( 'yith-pt-frontend-shortcode-css' );
			wp_enqueue_style( 'yith-pt-frontend-fontawesome-all' );
			wp_enqueue_style( 'yith-pt-frontend-fontawesome' );

			$testimonials = get_transient( 'yith_pt_latest_three_testimonials' );

			if ( ! $testimonials ) {

				$query = array(
					'numberposts'    => 3,
					'post_type'      => 'testimonial',
					'publish_status' => 'published',
					'orderby'        => 'date',
					'order'          => 'DESC',
				);

				$posts = get_posts( $query );

				if ( ! empty( $posts ) ) {

					set_transient( 'yith_pt_latest_three_testimonials', $posts, 3 * HOUR_IN_SECONDS );

				};

			}

			foreach ( $testimonials as $t ) {

				ob_start();
				yith_pt_get_template(
					'/frontend/show_post.php',
					array(
						'post'                   => $t,
						'show_photo'             => get_option( 'yith_pt_shortcode_show_photo' ),
						'hover_effect'           => get_option( 'yith_pt_shortcode_print_testimonials_hover_effect' ),
						'role'                   => get_post_meta( $t->ID, '_yith_pt_role', true ),
						'company'                => get_post_meta( $t->ID, '_yith_pt_company', true ),
						'company_url'            => get_post_meta( $t->ID, '_yith_pt_company_url', true ),
						'email'                  => get_post_meta( $t->ID, '_yith_pt_email', true ),
						'rating'                 => get_post_meta( $t->ID, '_yith_pt_rating', true ),
						'vip'                    => get_post_meta( $t->ID, '_yith_pt_vip', true ),
						'badge'                  => get_post_meta( $t->ID, '_yith_pt_badge', true ),
						'badge_text'             => get_post_meta( $t->ID, '_yith_pt_badge_text', true ),
						'badge_background_color' => get_post_meta( $t->ID, '_yith_pt_badge_background_color', true ),
						'links_color'            => get_option( 'yith_pt_shortcode_testimonials_links_color' ),
						'border_radius'          => get_option( 'yith_pt_shortcode_show_testimonials_border_radius' ),
					)
				);

			}

			return ob_get_clean();

		}

	}
}
