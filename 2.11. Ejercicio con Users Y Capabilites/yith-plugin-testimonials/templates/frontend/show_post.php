<?php
/**
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

?>

<div class="yith-pt-post-container 
	<?php
	if ( 'zoom' === $hover_effect ) {
		echo 'zoom ';
	} else {
		if ( 'highlight' === $hover_effect ) {
			echo 'highlight ';
		} else {
			echo '';
		}
	}
	if ( isset( $vip ) && 'yes' === $vip ) {
		echo 'vip ';
	}
	?>
	" 
	<?php
	if ( isset( $border_radius ) && '' !== $border_radius ) {
		echo 'style="border-radius:' . esc_attr( $border_radius ) . 'px;"';
	}
	?>
	>
	<?php
	if ( isset( $badge ) ) {
		if ( 'yes' === $badge ) {
			if ( isset( $badge_text ) && '' !== $badge_text ) {
				?>
				<div class="badge"><p class="badge-text" style="background-color: <?php echo esc_attr( $badge_background_color ); ?>;"><?php echo esc_html( strtoupper( $badge_text ) ); ?></p></div>	
			<?php } else { ?>
				<div class="badge clean"><p class="badge-text clean">a</p></div>
				<?php
			}
		} else {
			?>
			<div class="badge clean"><p class="badge-text clean">a</p></div>
			<?php
		}
	}

	if ( 'yes' === $show_photo ) {
		?>
		<div class="yith-pt-post-container-photo">
		<?php
			echo get_the_post_thumbnail( $post->ID, 'thumbnail', array( 'class' => 'aligncenter' ) );
		?>
		</div>
		<?php
	}
	?>

	<div class="yith-pt-post-title">
	<p>
	<?php echo esc_html( $post->post_title ); ?>
	</p>

	<?php
	if ( isset( $role ) && '' !== $role ) {
		?>
		<p class="yith-pt-role-and-company"> 
		<?php
			echo esc_html( strtoupper( $role ) );
		if ( isset( $company ) && '' !== $company ) {
			echo ' at ';
			if ( isset( $company_url ) && '' !== $company_url ) {
				echo '<a href="' . esc_attr( $company_url ) . '" target="_blank" style="color:' . esc_attr( $links_color ) . ';">' . esc_html( ucfirst( $company ) ) . '</a>';
			} else {
				echo esc_html( ucfirst( $company ) );
			}
		}
		?>
		</p>
	<?php } else { ?>
		<p class="yith-pt-role-and-company clean">a</p>
		<?php
	}

	if ( isset( $email ) && '' !== $email ) {
		echo '<a href="mailto:' . esc_attr( $email ) . '" style="color:' . esc_attr( $links_color ) . ';">' . esc_html( $email ) . '</a>';
	} else {
		echo '<a class="clean">a</a>';
	}
	?>
	</div>

	<?php if ( isset( $rating ) ) { ?>
		<div class="rating">
			<?php for ( $i = 1; $i <= $rating; $i++ ) { ?>
				<i class="fas fa-star star-color"></i>
			<?php } 
			if ( $rating < 5 ) {
				$stars_empty = 5 - $rating;
				for ( $i = 1; $i <= $stars_empty; $i++ ) {
					?>
					<i class="fas fa-star star-no-color"></i>	
					<?php
				}
			}
			?>
		</div>
	<?php } ?>

	<div class="yith-pt-post-content">
		<p>
		<?php echo '"' . esc_html( $post->post_content ) . '"'; ?>
		</p>
	</div>

</div>
