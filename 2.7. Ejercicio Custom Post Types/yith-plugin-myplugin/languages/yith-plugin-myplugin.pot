# Copyright (C) 2021 Juan Coronel Mendez
# This file is distributed under the same license as the YITH Plugin My Plugin plugin.
msgid ""
msgstr ""
"Project-Id-Version: YITH Plugin My Plugin 1.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/yith-plugin-myplugin\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-03-16T01:19:38+01:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.4.0\n"
"X-Domain: yith-plugin-myplugin\n"

#. Plugin Name of the plugin
msgid "YITH Plugin My Plugin"
msgstr ""

#. Description of the plugin
msgid "My Plugin for YITH Plugins"
msgstr ""

#. Author of the plugin
msgid "Juan Coronel Mendez"
msgstr ""

#. Author URI of the plugin
msgid "https://yithemes.com/"
msgstr ""

#: includes/class-yith-pmp-admin.php:69
msgid "Additional information"
msgstr ""

#: includes/class-yith-pmp-admin.php:125
msgid "Column1"
msgstr ""

#: includes/class-yith-pmp-admin.php:126
msgid "Column2"
msgstr ""

#: includes/class-yith-pmp-admin.php:173
#: includes/class-yith-pmp-admin.php:174
msgid "Plugin MyPlugin Options"
msgstr ""

#: includes/class-yith-pmp-admin.php:200
msgid "Number"
msgstr ""

#: includes/class-yith-pmp-admin.php:205
msgid "Show image"
msgstr ""

#: includes/class-yith-pmp-admin.php:212
msgid "Section"
msgstr ""

#: includes/class-yith-pmp-post-types.php:111
msgctxt "taxonomy general name"
msgid "Hierarchical taxonomy"
msgstr ""

#: includes/class-yith-pmp-post-types.php:112
msgctxt "taxonomy singular name"
msgid "Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:113
msgid "Search Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:114
msgid "All Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:115
msgid "Parent Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:116
msgid "Parent Hierarchical:"
msgstr ""

#: includes/class-yith-pmp-post-types.php:117
msgid "Edit Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:118
msgid "Update Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:119
msgid "Add New Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:120
msgid "New Hierarchical Name"
msgstr ""

#: includes/class-yith-pmp-post-types.php:121
msgid "Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:138
msgctxt "taxonomy general name"
msgid "No Hierarchical taxonomy"
msgstr ""

#: includes/class-yith-pmp-post-types.php:139
msgctxt "taxonomy singular name"
msgid "No Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:140
msgid "Search No Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:141
msgid "All No Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:142
msgid "Parent No Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:143
msgid "Parent No Hierarchical:"
msgstr ""

#: includes/class-yith-pmp-post-types.php:144
msgid "Edit No Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:145
msgid "Update No Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:146
msgid "Add New No Hierarchical"
msgstr ""

#: includes/class-yith-pmp-post-types.php:147
msgid "New No Hierarchical Name"
msgstr ""

#: includes/class-yith-pmp-post-types.php:148
msgid "No Hierarchical"
msgstr ""

#: views/admin/plugin-options-panel.php:9
msgid "Settings"
msgstr ""

#: views/metaboxes/plugin-myplugin-info-metabox.php:8
msgid "isbn"
msgstr ""

#: views/metaboxes/plugin-myplugin-info-metabox.php:13
msgid "price"
msgstr ""

#: views/metaboxes/plugin-myplugin-info-metabox.php:18
msgid "cover_type"
msgstr ""

#: views/metaboxes/plugin-myplugin-info-metabox.php:23
msgid "language"
msgstr ""
