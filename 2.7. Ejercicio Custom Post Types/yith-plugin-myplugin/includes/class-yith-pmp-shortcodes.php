<?php
/**
 * This file belongs to the YITH PMP Plugin My Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PMP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PMP_Shortcodes' ) ) {
	/**
	 * YITH_PMP_Shortcodes
	 */
	class YITH_PMP_Shortcodes {

		/**
		 * Main Instance
		 *
		 * @var YITH_PMP_Shortcodes
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PMP_Post_Types Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PMP_Post_Types constructor.
		 */
		private function __construct() {

			$shortcodes = array(
				'yith_pmp_show_post_type'    => __CLASS__ . '::show_post_types', // print post.
				'yith_pmp_show_book_metabox' => __CLASS__ . '::show_book_metabox', // print metabox.
			);
			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}

		}

		/**
		 * Show_post_types
		 *
		 * @param  args $args .
		 * @return string .
		 */
		public static function show_post_types( $args ) {

			// Enqueue custom CSS for the shortcode.
			wp_enqueue_style( 'yith-pmp-frontend-shortcode-css' );

			$args = shortcode_atts(
				array(
					'number_books' => get_option( 'yith_pmp_shortcode_show_books_to_search' ),
					'show_image'   => get_option( 'yith_pmp_shortcode_show_image' ),
					'post_type'    => 'pmp-myplugin',
				),
				$args,
				'yith_auction_products',
			);

			$posts = get_posts( $args );

			ob_start();

			foreach ( $posts as $post ) {
				yith_pmp_get_template(
					'/frontend/show_post.php',
					array(
						'post'                 => $post,
						'show_books_to_search' => $args['number_books'],
					)
				);
			}

			return '<div class="yith-pmp-posts">' . ob_get_clean() . '</div>';

		}

		/**
		 * Show_book_metabox
		 *
		 * @param args $args .
		 * @return string $result
		 */
		public function show_book_metabox( $args ) {
			$args  = array(
				'post_type'      => 'book',
				'publish_status' => 'published',
				'number_books'   => get_option( 'yith_pmp_shortcode_show_books_to_search' ),
				'show_image'     => get_option( 'yith_pmp_shortcode_show_image' ),
			);
			$query = new WP_Query( $args );

			if ( $query->have_posts() ) :

				$result       = '';
				$number_books = 1;

				while ( $query->have_posts() && $number_books <= $args['number_books'] ) :

					$query->the_post();
					$result .= '<div>' . get_the_title() . '</div>';
					if ( 'yes' === $args['show_image'] ) {
						$result .= '<div>' . get_the_post_thumbnail() . '</div>';
					}
					$result .= '<div>ISBN: ' . get_post_meta( get_the_ID(), '_yith_pmp_isbn', true ) . '</div>';
					$result .= '<div>Price: ' . get_post_meta( get_the_ID(), '_yith_pmp_price', true ) . '</div>';
					$result .= '<div>Cover type: ' . get_post_meta( get_the_ID(), '_yith_pmp_cover_type', true ) . '</div>';
					$result .= '<div>Language: ' . get_post_meta( get_the_ID(), '_yith_pmp_language', true ) . '</div>';
					$result .= '<div> ============ </div>';
					$number_books++;
				endwhile;

				wp_reset_postdata();

			endif;

			return $result;
		}

	}
}
