<?php
/**
 * This file belongs to the YITH PMP Plugin myplugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PMP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PMP_Post_Types' ) ) {
	/**
	 * YITH_PMP_Post_Types
	 */
	class YITH_PMP_Post_Types {

		/**
		 * Main Instance
		 *
		 * @var YITH_PMP_Post_Types
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Post type name
		 *
		 * @var YITH_PMP_Post_Types
		 * @since 1.0
		 * @access public
		 */
		public static $post_type = 'book';

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PMP_Post_Types Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PMP_Post_Types constructor.
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'yith_pmp_create_post_type_book' ) );
			add_action( 'init', array( $this, 'register_taxonomy' ) );
		}

		/**
		 * Setup the 'My Plugin' custom post type
		 */
		/*public function setup_post_type() {
			$args = array(
				'label'        => __( 'MyPlugin', 'yith-plugin-myplugin' ),
				'description'  => __( 'MyPlugin post type', 'yith-plugin-myplugin' ),
				'public'       => true,
				'menu_icon'    => 'dashicons-universal-access',
				'show_in_menu' => true,
				'show_ui'      => true,
				'rewrite'      => false,
				'supports'     => array( 'title', 'editor', 'author', 'thumbnail' ),
			);
			register_post_type( self::$post_type, $args );
		}*/

		/**
		 * Yith_pmp_create_post_type_book
		 *
		 * @return void
		 */
		public function yith_pmp_create_post_type_book() {
			/*$labels = array(
				'name'        => _x( 'Book', 'Post type general name', 'yith-plugin-myplugin' ),
				'description' => _x( 'Book', 'Post type description', 'yith-plugin-myplugin' ),
				'img'         => _x( 'Book', 'Post type img', 'yith-plugin-myplugin' ),
			);*/

				$args = array(
					'label'        => __( 'Book', 'yith-plugin-myplugin' ),
					'description'  => __( 'Book post type', 'yith-plugin-myplugin' ),
					'public'       => true,
					'menu_icon'    => 'dashicons-universal-access',
					'show_in_menu' => true,
					'show_ui'      => true,
					'rewrite'      => false,
					'supports'     => array( 'title', 'editor', 'author', 'thumbnail' ),
				);
				register_post_type( self::$post_type, $args );
		}

		/**
		 * Register_taxonomy
		 *
		 * @return void
		 */
		public function register_taxonomy() {
			// Add new taxonomy, make it hierarchical (like categories).
			$labels = array(
				'name'              => _x( 'Hierarchical taxonomy', 'taxonomy general name', 'yith-plugin-myplugin' ),
				'singular_name'     => _x( 'Hierarchical', 'taxonomy singular name', 'yith-plugin-myplugin' ),
				'search_items'      => __( 'Search Hierarchical', 'yith-plugin-myplugin' ),
				'all_items'         => __( 'All Hierarchical', 'yith-plugin-myplugin' ),
				'parent_item'       => __( 'Parent Hierarchical', 'yith-plugin-myplugin' ),
				'parent_item_colon' => __( 'Parent Hierarchical:', 'yith-plugin-myplugin' ),
				'edit_item'         => __( 'Edit Hierarchical', 'yith-plugin-myplugin' ),
				'update_item'       => __( 'Update Hierarchical', 'yith-plugin-myplugin' ),
				'add_new_item'      => __( 'Add New Hierarchical', 'yith-plugin-myplugin' ),
				'new_item_name'     => __( 'New Hierarchical Name', 'yith-plugin-myplugin' ),
				'menu_name'         => __( 'Hierarchical', 'yith-plugin-myplugin' ),
			);

			$args = array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'yith_tax_hierarchical' ),
			);

			register_taxonomy( 'yith_pmp_hietatchical_tax', array( self::$post_type ), $args );

			// Add new taxonomy, make it no hierarchical (like tags).

			$labels1 = array(
				'name'              => _x( 'No Hierarchical taxonomy', 'taxonomy general name', 'yith-plugin-myplugin' ),
				'singular_name'     => _x( 'No Hierarchical', 'taxonomy singular name', 'yith-plugin-myplugin' ),
				'search_items'      => __( 'Search No Hierarchical', 'yith-plugin-myplugin' ),
				'all_items'         => __( 'All No Hierarchical', 'yith-plugin-myplugin' ),
				'parent_item'       => __( 'Parent No Hierarchical', 'yith-plugin-myplugin' ),
				'parent_item_colon' => __( 'Parent No Hierarchical:', 'yith-plugin-myplugin' ),
				'edit_item'         => __( 'Edit No Hierarchical', 'yith-plugin-myplugin' ),
				'update_item'       => __( 'Update No Hierarchical', 'yith-plugin-myplugin' ),
				'add_new_item'      => __( 'Add New No Hierarchical', 'yith-plugin-myplugin' ),
				'new_item_name'     => __( 'New No Hierarchical Name', 'yith-plugin-myplugin' ),
				'menu_name'         => __( 'No Hierarchical', 'yith-plugin-myplugin' ),
			);

			$args1 = array(
				'hierarchical'      => false,
				'labels'            => $labels1,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'yith_tax_no_hierarchical' ),
			);

			register_taxonomy( 'yith_pmp_no_hietatchical_tax', array( self::$post_type ), $args1 );

		}

	}
}
