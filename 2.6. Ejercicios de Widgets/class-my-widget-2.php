<?php
/**
 * Plugin Name: My Widget 2
 * Plugin URI: http://wordpress.org/extend/plugins/#
 * Description: This is an example plugin
 * Author: Juan Coronel
 * Version: 1.0
 * Author URI: http://example.com/
 *
 * @package .
 */

// register My_Widget.
add_action(
    'widgets_init',
    function() {
        register_widget( 'My_Widget_2' );
    },
);

/**
 * My_Widget
 */
class My_Widget_2 extends WP_Widget {
    /**
     * __construct.
     *
     * @return void
     */
    public function __construct() {
        $widget_ops = array(
            'classname'   => 'my_widget_2',
            'description' => 'Mi segundo plugin',
        );
        parent::__construct( 'my_widget_2', 'Mi segundo widget', $widget_ops );
    }


    /**
     * Output the widget content on the front-end.
     *
     * @param  mixed $args .
     * @param  mixed $instance .
     *
     */
    public function widget( $args, $instance ) {
        
        $query = array(
            'showposts'           => 5,
            'orderby'             => 'date',
            'order'               => 'DESC',
            'post_status'         => 'publish',
            'ignore_sticky_posts' => 1,
        );
        $q     = new WP_Query( $query );
        if ( $q->have_posts() ) {
            $salida  = '';
            $salida .= '<ul> Ultimos 5 posts:';
            $i       = 0;
        }
        while ( $q->have_posts() ) {
            $q->the_post();
            $i++;
            $salida .= '<li>' . $i . ': ' . get_the_title() . ', fecha de publicacion: ' . get_the_date( 'd/m/Y' ) . '</li>';
        }
        $salida .= '</ul>';
        echo esc_html( $salida );
    }
    
    
    /**
     * Output the option form field in admin Widgets screen.
     *
     * @param mixed $instance .
     * @return void
     */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Title', 'text_domain' );
        ?>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
                <?php esc_attr_e( 'Title:', 'text_domain' ); ?>
            </label>
    
            <input
                class="widefat"
                id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
                type="text"
                value="<?php echo esc_attr( $title ); ?>">
        <?php
    }
    
    /**
     * Save options.
     *
     * @param  mixed $new_instance .
     * @param  mixed $old_instance .
     * @return string $instance .
     */
    public function update( $new_instance, $old_instance ) {
        $instance                   = array();
        $instance['title']          = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $selected_posts             = ( ! empty( $new_instance['selected_posts'] ) ) ? (array) $new_instance['selected_posts'] : array();
        $instance['selected_posts'] = array_map( 'sanitize_text_field', $selected_posts );

        return $instance;
    }

}
