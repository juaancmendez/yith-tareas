<?php
/**
 * Plugin Name: My Widget
 * Plugin URI: http://wordpress.org/extend/plugins/#
 * Description: This is an example plugin
 * Author: Juan Coronel
 * Version: 1.0
 * Author URI: http://example.com/
 *
 * @package .
 */

// register My_Widget.
add_action(
    'widgets_init',
    function() {
        register_widget( 'My_Widget' );
    },
);

/**
 * My_Widget
 */
class My_Widget extends WP_Widget {
    /**
     * __construct.
     *
     * @return void
     */
    public function __construct() {
        $widget_ops = array(
            'classname'   => 'my_widget',
            'description' => 'Mi primer plugin',
        );
        parent::__construct( 'my_widget', 'Mi primer widget', $widget_ops );
    }


    /**
     * Output the widget content on the front-end.
     *
     * @param  mixed $args .
     * @param  mixed $instance .
     * @return void
     */
    public function widget( $args, $instance ) {
        
        $content_widget  = '';
        $content_widget .= '
        <form>
            <label for="txt-nombre">Nombre: </label>
            <input type="text" name="txt-nombre" id="txt-nombre">
            <label for="txt-apellido">Apellido: </label>
            <input type="text" name="txt-apellido" id="txt-apellido">
            <label for="txt-direccion">Direccion: </label>
            <input type="text" name="txt-direccion" id="txt-direccion">
            <label for="txt-telefono">Telefono: </label>
            <input type="number" name="txt-telefono" id="txt-telefono">
            <button type="">Enviar</button>
        </form>';
    
        echo esc_html( $content_widget );
    }
    
    
    /**
     * Output the option form field in admin Widgets screen.
     *
     * @param mixed $instance .
     * @return void
     */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Title', 'text_domain' );
        ?>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
                <?php esc_attr_e( 'Title:', 'text_domain' ); ?>
            </label>
    
            <input
                class="widefat"
                id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
                type="text"
                value="<?php echo esc_attr( $title ); ?>">
        <?php
    }
        
    /**
     * Save options.
     *
     * @param  mixed $new_instance .
     * @param  mixed $old_instance .
     * @return string $instance .
     */
    public function update( $new_instance, $old_instance ) {
        $instance                   = array();
        $instance['title']          = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $selected_posts             = ( ! empty( $new_instance['selected_posts'] ) ) ? (array) $new_instance['selected_posts'] : array();
        $instance['selected_posts'] = array_map( 'sanitize_text_field', $selected_posts );

        return $instance;
    }

}
