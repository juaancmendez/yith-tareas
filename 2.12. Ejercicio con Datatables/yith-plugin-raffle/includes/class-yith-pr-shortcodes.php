<?php
/**
 * This file belongs to the YITH PR Plugin Raffle.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PR_Shortcodes' ) ) {
	/**
	 * YITH_PR_Shortcodes
	 */
	class YITH_PR_Shortcodes {

		/**
		 * Main Instance
		 *
		 * @var YITH_PR_Shortcodes
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PR_Post_Types Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PR_Post_Types constructor.
		 */
		private function __construct() {

			$shortcodes = array(
				'yith_pr_show_raffle' => __CLASS__ . '::show_raffle', // print raffle.
			);

			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}

		}

		/**
		 * Print the raffle structure.
		 *
		 * @return string $raffle_structure
		 */
		public static function show_raffle() {

			wp_enqueue_style( 'yith-pr-frontend-shortcode-css' );
			$user_id = is_user_logged_in() ? get_current_user_id() : 0;

			$raffle_structure  = '';
			$raffle_structure .=
				'<div class="yith-pr-raffle-container"';
				if ( 0 !== $user_id ) {
					$raffle_structure .= ' data-id="' . $user_id . '">';
				} else {
					$raffle_structure .= '>';
				}
			if ( ! is_user_logged_in() ) {
				$raffle_structure .=
					'<form method="post"><label for="txtNombre">Nombre: </label>
					 <input type="text" name="txtNombre" id="txtNombre">
					 <label for="txtApellido">Apellido: </label>
					 <input type="text" name="txtApellido" id="txtApellido">
					 <label for="txtEmail">Email: </label>
					 <input type="email" name="txtEmail" id="txtEmail">';
			}
			$raffle_structure .=
					'<input type="checkbox" name="chkRaffle" id="chkRaffle"><label for="chkRaffle">Si, quiero participar en el sorteo.</label><br>
					 <button type="submit" id="yith-pr-button">Inscribirse</button></form>
				 </div>';

			return $raffle_structure;

		}

	}
}
