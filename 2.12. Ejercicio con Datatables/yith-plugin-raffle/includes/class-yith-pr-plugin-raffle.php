<?php
/**
 * This file belongs to the YITH PR Plugin Raffle.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PR_Plugin_Raffle' ) ) {
	/**
	 * YITH_PR_Plugin_Raffle
	 */
	class YITH_PR_Plugin_Raffle {

		/**
		 * Main Instance
		 *
		 * @var YITH_PR_Plugin_Raffle
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main Admin Instance
		 *
		 * @var YITH_PR_Plugin_Raffle_Admin
		 * @since 1.0
		 */
		public $admin = null;
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_PR_Plugin_Raffle_Frontend
		 * @since 1.0
		 */
		public $frontend = null;

		/**
		 * Main Shortcodes Instance
		 *
		 * @var YITH_PR_Plugin_Raffle_Frontend
		 *
		 * @since 1.0
		 */
		public $shortcodes = null;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PR_Plugin_Raffle Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

			// ternary operator --> https://www.codementor.io/@sayantinideb/ternary-operator-in-php-how-to-use-the-php-ternary-operator-x0ubd3po6 .
		}
		/**
		 * YITH_PR_Plugin_Raffle constructor.
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_pr_require_class',
				array(
					'common'   => array(
						'includes/class-yith-pr-post-types.php',
						'includes/functions.php',
						'includes/class-yith-pr-shortcodes.php',
						'includes/class-yith-pr-ajax.php',
						// 'includes/class-yith-pr-compatibility.php',
						// 'includes/class-yith-pr-other-class.php',
					),
					'admin'    => array(
						'includes/class-yith-pr-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-pr-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			/*
				Here set any other hooks ( actions or filters you'll use on this class)
			*/
			// Finally call the init function.
			$this->init();

		}

		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param array? $main_classes array The require classes file path.
		 *
		 * @author Juan Coronel
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_PR_DIR_PATH . $class ) ) {
						require_once( YITH_PR_DIR_PATH . $class );
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 *
		 * @author Juan Coronel
		 *
		 * @since  1.0
		 **/
		public function init_classes() {
			// $this->function = YITH_PR_Other_Class::get_instance();
			$this->ajax = YITH_PR_Ajax::get_instance();
			// $this->compatibility = YITH_PR_Compatibility::get_instance();
			$this->shortcodes = YITH_PR_Shortcodes::get_instance();
			YITH_PR_Post_Types::get_instance();
		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 *
		 * @author Juan Coronel Mendez
		 * @since  1.0
		 * @return void
		 * @access protected
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_PR_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_PR_Frontend::get_instance();
			}
		}

	}
}
/**
 * Get the YITH_PR_Plugin_Raffle instance
 *
 * @return YITH_PR_Plugin_Raffle
 */
if ( ! function_exists( 'yith_pr_plugin_raffle' ) ) {
	/**
	 * Yith_pr_plugin_raffle
	 *
	 * @return Object?
	 */
	function yith_pr_plugin_raffle() {
		return YITH_PR_Plugin_Raffle::get_instance();
	}
}
