<?php
/**
 * This file belongs to the YITH PR Plugin Raffle.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PR_Admin' ) ) {
	/**
	 * YITH_PR_Admin
	 */
	class YITH_PR_Admin {
		/**
		 * Main Instance
		 *
		 * @var YITH_PR_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PR_Admin Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PR_Admin constructor.
		 */
		private function __construct() {

			//add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
			//add_action( 'save_post', array( $this, 'save_meta_box' ) );

			// See definition on class-wp-post-list-table.php  manage_{$post_type}_posts_columns; .
			//add_filter( 'manage_raffle_posts_columns', array( $this, 'add_raffle_post_type_columns' ) );

			// See definition on class-wp-post-list-table.php manage_{$post->post_type}_posts_custom_column .
			// add_action( 'manage_raffle_posts_custom_column', array( $this, 'display_raffle_post_type_custom_column' ), 10, 2 );

			// Admin menu.

			add_action( 'admin_menu', array( $this, 'create_menu_for_general_options' ) );
			//add_action( 'admin_init', array( $this, 'register_settings' ) );

		}


		/**
		 * Setup the meta boxes
		 */
		/*public function add_meta_boxes() {
			add_meta_box(
				'yith-pr-additional-information',
				__(
					'Additional information',
					'yith-plugin-raffle'
				),
				array( $this, 'view_meta_boxes' ),
				YITH_PR_Post_Types::$post_type
			);
		}*/

		/**
		 * Wiev meta boxes
		 *
		 * @param string? $post .
		 */
		/*public function view_meta_boxes( $post ) {
			// yith_pr_get_view( '/metaboxes/plugin-raffle-info-metabox.php', array( 'post' => $post ) );
		}*/

		/**
		 * Save meta box values
		 *
		 * @param string? $post_id .
		 */
		/*public function save_meta_box( $post_id ) {

			if ( YITH_PR_Post_Types::$post_type !== get_post_type( $post_id ) ) {
				return;
			}

		}*/
		/**
		 * Filters the columns displayed in the Posts list table for plugin myplugin post type.
		 *
		 * @param string[] $post_columns An associative array of column headings.
		 */
		//public function add_raffle_post_type_columns( $post_columns ) {

			//return $post_columns;

		//}
		/**
		 *  Create menu for general options
		 */
		public function create_menu_for_general_options() {

			// See the following option https://developer.wordpress.org/reference/functions/add_menu_page/ .
				add_menu_page(
					esc_html__( 'Plugin Raffle Options', 'yith-plugin-raffle' ),
					esc_html__( 'Plugin Raffle Options', 'yith-plugin-raffle' ),
					'manage_options',
					'plugin_raffle_options',
					array( $this, 'raffle_custom_menu_page' ),
					'',
					40
				);

		}

	}
}
