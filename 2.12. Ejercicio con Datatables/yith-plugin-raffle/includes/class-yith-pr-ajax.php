<?php
/**
 * This file belongs to the YITH PR Plugin Raffle.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PR_Ajax' ) ) {
	/**
	 * YITH_PR_Ajax
	 */
	class YITH_PR_Ajax {
		/**
		 * Main Instance
		 *
		 * @var YITH_PR_Ajax
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PR_Ajax Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PR_Ajax constructor.
		 */
		private function __construct() {

			add_action( 'wp_ajax_raffle_action', array( $this, 'raffle_action' ) );
			add_action( 'wp_ajax_nopriv_raffle_action', array( $this, 'raffle_action' ) );

		}
		/**
		 * Función que procesa la llamada AJAX
		 *
		 * @return void
		 */
		public function testimonials_notify_button_click() {

			// Check parameters
			error_log( 'entre a la funcion' );
			$message = isset( $_POST['message'] ) ? $_POST['message'] : 'false';
			if ( ! $message ) {

				wp_send_json( array( 'message' => __( 'Message not received :(', 'wpduf' ) ) );

			} else {

				wp_send_json( array( 'message' => __( 'Message received, greetings from server!', 'wpduf' ) ) );

			}

		}
		/**
		 * Testimonials_vote_up
		 *
		 * @return void
		 */
		public function raffle_action() {

			$arr_raffle_data = isset( $_POST['message'] ) ? $_POST['message'] : 'false';

			if ( $arr_raffle_data['user_id'] ) {
				$user                        = get_user_by( 'ID', $arr_raffle_data['user_id'] );
				$arr_raffle_data['nombre']   = $user->nickname;
				$arr_raffle_data['apellido'] = $user->last_name;
				$arr_raffle_data['email']    = $user->user_email;
			}
			error_log($user->mail);
			global $wpdb;
			$registros = $wpdb->get_results( 'SELECT email FROM wp_raffle WHERE email LIKE "' . $arr_raffle_data['email'] . '"' );

			if ( ! $registros ) {
				$wpdb->insert(
					'wp_raffle',
					array(
						'id_usuario'  => $arr_raffle_data['user_id'] ? $arr_raffle_data['user_id'] : null,
						'nombre'   => $arr_raffle_data['nombre'],
						'apellido' => $arr_raffle_data['apellido'],
						'email'    => $arr_raffle_data['email'],
					)
				);
				wp_send_json( array( 'ok' => __( 'Te has inscripto con exito!', 'wpduf' ) ) );
			} else {
				wp_send_json( array( 'error' => __( 'Ya has votado con este mail.', 'wpduf' ) ) );
			}

		}

	}
}
