<?php
/**
 * This file belongs to the YITH PR Plugin Raffle.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

// HERE SET FUNCTIONS TO USE ON YOUR PROJECT LIKE Template functions.

/**
 * Include templates
 *
 * @param $file_name name of the file you want to include.
 * @param array $args (array) (optional) Arguments to retrieve.
 */
if ( ! function_exists( 'yith_pr_get_template' ) ) {
	/**
	 * Yith_pr_get_template
	 *
	 * @param  string $file_name .
	 * @param  array? $args .
	 * @return void
	 */
	function yith_pr_get_template( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_PR_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

/**
 * Include views
 *
 * @param $file_name name of the file you want to include.
 * @param array $args (array) (optional) Arguments to retrieve.
 */
if ( ! function_exists( 'yith_pr_get_view' ) ) {
	/**
	 * Yith_pr_get_view
	 *
	 * @param  string $file_name .
	 * @param  array  $args .
	 * @return void
	 */
	function yith_pr_get_view( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_PR_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}
if ( ! function_exists( 'yith_pr_create_raffle_db' ) ) {
	/**
	 * Yith_pr_create_raffle_db
	 *
	 * @return void
	 */
	function yith_pr_create_raffle_db() {

		global $wpdb;

		// Con esto creamos el nombre de la tabla y nos aseguramos que se cree con el mismo prefijo que ya tienen las otras tablas creadas (wp_raffle).
		$table_name = $wpdb->prefix . 'raffle';

		// Declaramos la tabla que se creará de la forma común.
		$sql = "CREATE TABLE $table_name (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`id_usuario` int(11),
			`nombre` varchar(255) NOT NULL,
			`apellido` varchar(255) NOT NULL,
			`email` varchar(255) NOT NULL,
			UNIQUE KEY id (id)
		);";
		// upgrade contiene la función dbDelta la cuál revisará si existe la tabla.
		require_once ABSPATH . '/wp-admin/includes/upgrade.php';
		// Creamos la tabla
		dbDelta( $sql );

	}
}
// Ejecutamos nuestra funcion en WordPress.
add_action( 'init', 'yith_pr_create_raffle_db' );
