<?php
/**
 * Plugin Name: YITH Plugin Raffle
 * Description: Raffle for YITH Plugins
 * Version: 1.0.0
 * Author: Juan Coronel Mendez
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-raffle
 *
 * @package .
 */

! defined( 'ABSPATH' ) && exit;   // Before all, check if defined ABSPATH.

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'YITH_PR_VERSION' ) ) {
	define( 'YITH_PR_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PR_DIR_URL' ) ) {
	define( 'YITH_PR_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PR_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PR_DIR_ASSETS_URL', YITH_PR_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PR_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PR_DIR_ASSETS_CSS_URL', YITH_PR_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PR_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PR_DIR_ASSETS_JS_URL', YITH_PR_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PR_DIR_PATH' ) ) {
	define( 'YITH_PR_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PR_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PR_DIR_INCLUDES_PATH', YITH_PR_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PR_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PR_DIR_TEMPLATES_PATH', YITH_PR_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PR_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PR_DIR_VIEWS_PATH', YITH_PR_DIR_PATH . 'views' );
}

if ( ! defined( 'HOUR_IN_SECONDS' ) ) {
	define( 'HOUR_IN_SECONDS', 3600 );
}

// Different way to declare a constant.

! defined( 'YITH_PR_INIT' ) && define( 'YITH_PR_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_PR_SLUG' ) && define( 'YITH_PR_SLUG', 'yith-plugin-raffle' );
! defined( 'YITH_PR_SECRETKEY' ) && define( 'YITH_PR_SECRETKEY', 'zd9egFgFdF1D8Azh2ifA' );
! defined( 'YITH_PR_OPTIONS_PATH' ) && define( 'YITH_PR_OPTIONS_PATH', YITH_PR_DIR_PATH . 'plugin-options' );

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_pr_init_classes' ) ) {
	/**
	 * Yith_pr_init_classes
	 *
	 * @return void
	 */
	function yith_pr_init_classes() {

		load_plugin_textdomain( 'yith-plugin-raffle', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// Require all the files you include on your plugins. Example.
		require_once YITH_PR_DIR_INCLUDES_PATH . '/class-yith-pr-plugin-raffle.php';

		if ( class_exists( 'YITH_PR_Plugin_Raffle' ) ) {
			/*
			*	Call the main function
			*/
			yith_pr_plugin_raffle();
		}
	}
}

add_action( 'plugins_loaded', 'yith_pr_init_classes', 11 );

/*if ( ! function_exists( 'yith_raffle_install' ) ) {
	error_log('pase por !function_Exists');
	/**
	 * Yith_raffleinstall
	 *
	 * @return void
	 */
	/*function yith_raffle_install() {

		if ( ! function_exists( 'RF' ) ) {
			add_action( 'admin_notices', 'yith_raffle_install_admin_notice' );
		} else {
			do_action( 'yith_raffle_init' );
			YITH_RAFFLE_DB::install();  // This is the class where you can create the new datatable
		}

	}

	add_action( 'plugins_loaded', 'yith_raffle_install', 11 );

}

if ( ! class_exists( 'YITH_RAFFLE_DB' ) ) {
	/**
	 * YITH Raffle Database
	 *
	 * @since 1.0.0
	 */
	/*class YITH_RAFFLE_DB {
		/**
		 * DB version
		 *
		 * @var string $auction_table
		 */
		/*public static $version       = '1.0.0';
		public static $auction_table = 'yith_raffle_auction'; //phpcs:ignore

		/**
		 * Constructor
		 *
		 * @return YITH_WCBK_DB
		 */
		/*private function __construct() {
		}

		/**
		 * Installer DB
		 *
		 * @return void
		 */
		/*public static function install() {
			self::create_db_table();
		}

		/**
		 * Create table for Raffle
		 *
		 * @param bool $force
		 */
		/*public static function create_db_table( $force = false ) {

			global $wpdb;
			$current_version = get_option( 'yith_raffle_db_version' );

			if ( $force || $current_version != self::$version ) {

				$wpdb->hide_errors();
				$table_name      = $wpdb->prefix . self::$auction_table;  // The table always must have a prefix.
				$charset_collate = $wpdb->get_charset_collate();

				$sql
					= "CREATE TABLE $table_name (
					`id` bigint(20) NOT NULL AUTO_INCREMENT,
					`user_id` bigint(20) NOT NULL,
					`name` varchar(50) NOT NULL,
					`surname` varchar(50) NOT NULL,
					`email` varchar(50) NOT NULL,
					`auction_id` bigint(20) NOT NULL,
					`bid` varchar(255) NOT NULL,
					`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
					PRIMARY KEY (id)
					) $charset_collate;";

				if ( ! function_exists( 'dbDelta' ) ) {
					require_once ABSPATH . 'wp-admin/includes/upgrade.php';
				}

				dbDelta( $sql );  // Use DBdelta function for create the table.
				update_option( 'yith_raffle_db_version', self::$version );

			}

		}

	}
}*/
