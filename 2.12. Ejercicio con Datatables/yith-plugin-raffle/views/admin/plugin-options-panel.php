<?php
/**
 * 
 */

?>

<div class="wrap">
    <h1><?php esc_html_e( 'Settings', 'yith-plugin-raffle' ); ?></h1>

    <form method="post" action="options.php">
        <?php
            settings_fields( 'pr-options-page' );
		    do_settings_sections( 'pr-options-page' );
            submit_button();
        ?>

    </form>
</div>