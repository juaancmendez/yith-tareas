jQuery(document).ready( function(){
    // controles post click en boton "Suscribirse"
    jQuery('#yith-pr-button').on('click', function(){  
        if (jQuery('#chkRaffle').is(':checked')) {
            //alert('Bien');
            //alert(jQuery('#txtNombre').val())
            //La llamada AJAX
            var datos = {
                "user_id" : jQuery('.yith-pr-raffle-container').attr('data-id') ? jQuery('.yith-pr-raffle-container').attr('data-id') : "",
                "nombre" : jQuery('#txtNombre').val() ? jQuery('#txtNombre').val() : "",
                "apellido" : jQuery('#txtApellido').val() ? jQuery('#txtApellido').val() : "",
                "email" : jQuery('#txtEmail').val() ? jQuery('#txtEmail').val() : "",
            };
            //alert(datos['user_id'])
            jQuery.ajax({
                type : 'post',
                url : wp_ajax_tets_vars.ajax_url,
                data : {
                    action: 'raffle_action', 
                    message: datos
                },
                error: function(response){
                    console.log(response)
                    alert('Error')
                },
                success: function(response) {
                    // Actualiza el mensaje con la respuesta
                    if ( response['ok'] ) {
                        alert(response['ok']);
                    } else {
                        if ( response['error'] ) {
                            alert(response['error']);
                        }
                    }
                }
            })
        } else {
            alert('Para inscribirte, debes tildar tu consentimiento.');
        }
    });

});