<?php
/**
 * Twenty Twenty functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

/**
 * Create a shortcode that passes the post_id to it and returns the content.
 *
 * @param  mixed $atts .
 * @return void
 */
function twentytwenty_exercise_2_5_1( $atts ) {
	$atts      = shortcode_atts( array( 'value' => '' ), $atts );
	$v_content = get_the_content( null, null, $atts['value'] );
	echo esc_html( $v_content );
}

add_shortcode( 'show_content', 'twentytwenty_exercise_2_5_1' );

/**
 * Create a shortcode that lists the last 5 posts that have been created on the web.
 *
 * @param  mixed $atts .
 * @return void
 */
function twentytwenty_exercise_2_5_2( $atts ) {
	$atts  = shortcode_atts( array( 'value' => '' ), $atts );
	$query = array(
		'showposts'           => $atts['value'],
		'orderby'             => 'date',
		'order'               => 'DESC',
		'post_status'         => 'publish',
		'ignore_sticky_posts' => 1,
	);
	$q     = new WP_Query( $query );
	if ( $q->have_posts() ) {
		$salida  = '';
		$salida .= '<ul> Ultimos 5 posts:';
		$i       = 0;
	}
	while ( $q->have_posts() ) {
		$q->the_post();
		$i++;
		$salida .= '<li>' . $i . ': ' . get_the_title() . ', fecha de publicacion: ' . get_the_date( 'd/m/Y' ) . '</li>';
	}
	$salida .= '</ul>';
	echo esc_html( $salida );
}

add_shortcode( 'show_latest_post', 'twentytwenty_exercise_2_5_2' );

/**
 * Create a shortcode that shows the data of a user -> first name, last name, nickname.
 *
 * @param  mixed $atts .
 * @return void
 */
function twentytwenty_exercise_2_5_3( $atts ) {
	$atts = shortcode_atts( array( 'value' => '' ), $atts );
	$user = get_user_by( 'id', $atts['value'] );
	echo '<p>Usuario: ' . esc_html( $user->ID ) . '-> nombre: ' . esc_html( $user->first_name ) . ', apellido: ' . esc_html( $user->last_name ) . ', nick: ' . esc_html( $user->nickname ) . '</p>';
}

add_shortcode( 'show_data_user', 'twentytwenty_exercise_2_5_3' );

