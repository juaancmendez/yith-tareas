<?php
/**
 * 
 */

?>

<div class="wrap">
    <h1><?php esc_html_e( 'Settings', 'yith-plugin-myplugin' ); ?></h1>

    <form method="post" action="options.php">
        <?php
            settings_fields( 'pmp-options-page' );
		    do_settings_sections( 'pmp-options-page' );
            submit_button();
        ?>

    </form>
</div>