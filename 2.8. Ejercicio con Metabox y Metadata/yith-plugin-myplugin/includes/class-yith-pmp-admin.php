<?php
/**
 * This file belongs to the YITH PMP Plugin My Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PMP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PMP_Admin' ) ) {
	/**
	 * YITH_PMP_Admin
	 */
	class YITH_PMP_Admin {
		/**
		 * Main Instance
		 *
		 * @var YITH_PMP_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PMP_Admin Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PMP_Admin constructor.
		 */
		private function __construct() {

			add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
			add_action( 'save_post', array( $this, 'save_meta_box' ) );

			// See definition on class-wp-post-list-table.php  manage_{$post_type}_posts_columns; .
			add_filter( 'manage_book_posts_columns', array( $this, 'add_book_post_type_columns' ) );

			// See definition on class-wp-post-list-table.php manage_{$post->post_type}_posts_custom_column .
			add_action( 'manage_book_posts_custom_column', array( $this, 'display_book_post_type_custom_column' ), 10, 2 );

			// Admin menu.

			add_action( 'admin_menu', array( $this, 'create_menu_for_general_options' ) );
			add_action( 'admin_init', array( $this, 'register_settings' ) );

		}


		/**
		 * Setup the meta boxes
		 */
		public function add_meta_boxes() {
			add_meta_box(
				'yith-pmp-additional-information',
				__(
					'Additional information',
					'yith-plugin-myplugin'
				),
				array( $this, 'view_meta_boxes' ),
				YITH_PMP_Post_Types::$post_type
			);
		}

		/**
		 * Wiev meta boxes
		 *
		 * @param string? $post .
		 */
		public function view_meta_boxes( $post ) {
			yith_pmp_get_view( '/metaboxes/plugin-myplugin-info-metabox.php', array( 'post' => $post ) );
		}

		/**
		 * Save meta box values
		 *
		 * @param string? $post_id .
		 */
		public function save_meta_box( $post_id ) {

			if ( YITH_PMP_Post_Types::$post_type !== get_post_type( $post_id ) ) {
				return;
			}

			if ( isset( $_POST['_yith_pmp_isbn'] ) ) {
				update_post_meta( $post_id, '_yith_pmp_isbn', $_POST['_yith_pmp_isbn'] );
			}

			if ( isset( $_POST['_yith_pmp_price'] ) ) {
				update_post_meta( $post_id, '_yith_pmp_price', $_POST['_yith_pmp_price'] );
			}

			if ( isset( $_POST['_yith_pmp_cover_type'] ) ) {
				update_post_meta( $post_id, '_yith_pmp_cover_type', $_POST['_yith_pmp_cover_type'] );
			}

			if ( isset( $_POST['_yith_pmp_language'] ) ) {
				update_post_meta( $post_id, '_yith_pmp_language', $_POST['_yith_pmp_language'] );
			}

		}
		/**
		 * Filters the columns displayed in the Posts list table for plugin myplugin post type.
		 *
		 * @param string[] $post_columns An associative array of column headings.
		 */
		public function add_book_post_type_columns( $post_columns ) {

			$new_columns = apply_filters(
				'yith_pmp_myplugin_custom_columns',
				array(
					'isbn'       => esc_html__( 'ISBN', 'yith-plugin-myplugin' ),
					'price'      => esc_html__( 'Price', 'yith-plugin-myplugin' ),
					'cover_type' => esc_html__( 'Cover Type', 'yith-plugin-myplugin' ),
					'language'   => esc_html__( 'Language', 'yith-plugin-myplugin' ),
				)
			);

			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;
		}
		/**
		 * Fires for each custom column of a specific post type in the Posts list table.
		 *
		 * @param string $column_name The name of the column to display.
		 * @param int    $post_id     The current post ID.
		 * */
		public function display_book_post_type_custom_column( $column_name, $post_id ) {

			switch ( $column_name ) {

				case 'isbn':
					// Operations for get information for isbn example.
					echo 'value isbn for post ' . esc_html( $post_id );
				break;
				case 'price':
					// Operations for get information for price example.
					echo 'value price for post ' . esc_html( $post_id );
					break;
				case 'cover_type':
					// Operations for get information for cover_type example.
					echo 'value cover_type for post ' . esc_html( $post_id );
					break;
				case 'language':
					// Operations for get information for language example.
					echo 'value language for post ' . esc_html( $post_id );
					break;
				default:
						do_action( 'yith_pmp_myplugin_display_custom_column', $column, $post_id );
					break;
			}

		}
		/**
		 *  Create menu for general options
		 */
		public function create_menu_for_general_options() {

			// See the following option https://developer.wordpress.org/reference/functions/add_menu_page/ .
				add_menu_page(
					esc_html__( 'Plugin MyPlugin Options', 'yith-plugin-myplugin' ),
					esc_html__( 'Plugin MyPlugin Options', 'yith-plugin-myplugin' ),
					'manage_options',
					'plugin_myplugin_options',
					array( $this, 'myplugin_custom_menu_page' ),
					'',
					40
				);

		}
		/**
		 *  Callback custom menu page
		 */
		public function myplugin_custom_menu_page() {
			yith_pmp_get_view( '/admin/plugin-options-panel.php', array() );
		}

		/**
		 * Add the fields in the shortcode attribute management page
		 */
		public function register_settings() {			
			$page_name    = 'pmp-options-page';
			$section_name = 'options_section';

			$setting_fields = array(
				array(
					'id'       => 'yith_pmp_shortcode_show_image',
					'title'    => esc_html__( 'Show image', 'yith-plugin-myplugin' ),
					'callback' => 'print_show_image',
				),
				array(
					'id'       => 'yith_pmp_shortcode_show_books_to_search',
					'title'    => esc_html__( 'Books to search', 'yith-plugin-myplugin' ),
					'callback' => 'print_show_books_to_search',
				),
			);

			add_settings_section(
				$section_name,
				esc_html__( 'Section', 'yith-plugin-myplugin' ),
				'',
				$page_name
			);

			foreach ( $setting_fields as $field ) {
				extract( $field );

				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id )
				);

				register_setting( $page_name, $id );
			}

		}

		/**
		 * Print the show books_to_search toggle field
		 */
		public function print_show_books_to_search() {
			$tst_number = intval( get_option( 'yith_pmp_shortcode_show_books_to_search', 5 ) );
			?>
			<input type="number" id="yith_pmp_shortcode_show_books_to_search" name="yith_pmp_shortcode_show_books_to_search"
			value="<?php echo '' !== $tst_number ? esc_html( $tst_number ) : 6; ?>">
			<?php
		}

		/**
		 * Print the show image toggle field
		 */
		public function print_show_image() {
			?>
			<input type="checkbox" class="pmp-tst-option-panel__onoff__input" name="yith_pmp_shortcode_show_image"
				value='yes'
				id="yith_pmp_shortcode_show_image"
				<?php checked( get_option( 'yith_pmp_shortcode_show_image', '' ), 'yes' ); ?>
			>
			<label for="shortcode_show_image" class="pmp-tst-option-panel__onoff__label ">
				<span class="pmp-tst-option-panel__onoff__btn"></span>
			</label>
			<?php
		}

	}
}
