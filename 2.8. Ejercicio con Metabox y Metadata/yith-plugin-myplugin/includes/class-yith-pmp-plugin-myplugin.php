<?php
/**
 * This file belongs to the YITH PMP Plugin My Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PMP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PMP_Plugin_MyPlugin' ) ) {
	/**
	 * YITH_PMP_Plugin_MyPlugin
	 */
	class YITH_PMP_Plugin_MyPlugin {

		/**
		 * Main Instance
		 *
		 * @var YITH_PMP_Plugin_MyPlugin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main Admin Instance
		 *
		 * @var YITH_PMP_Plugin_MyPlugin_Admin
		 * @since 1.0
		 */
		public $admin = null;
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_PMP_Plugin_MyPlugin_Frontend
		 * @since 1.0
		 */
		public $frontend = null;

		/**
		 * Main Shortcodes Instance
		 *
		 * @var YITH_PMP_Plugin_MyPlugin_Frontend
		 *
		 * @since 1.0
		 */
		public $shortcodes = null;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PMP_Plugin_MyPlugin Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

			// ternary operator --> https://www.codementor.io/@sayantinideb/ternary-operator-in-php-how-to-use-the-php-ternary-operator-x0ubd3po6 .
		}
		/**
		 * YITH_PMP_Plugin_MyPlugin constructor.
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_pmp_require_class',
				array(
					'common'   => array(
						'includes/class-yith-pmp-post-types.php',
						'includes/functions.php',
						'includes/class-yith-pmp-shortcodes.php',
						// 'includes/class-yith-pmp-ajax.php',
						// 'includes/class-yith-pmp-compatibility.php',
						// 'includes/class-yith-pmp-other-class.php',
					),
					'admin' => array(
						'includes/class-yith-pmp-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-pmp-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			/*
				Here set any other hooks ( actions or filters you'll use on this class)
			*/
			// Finally call the init function.
			$this->init();

		}

		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param array? $main_classes array The require classes file path.
		 *
		 * @author Juan Coronel
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_PMP_DIR_PATH . $class ) ) {
						require_once( YITH_PMP_DIR_PATH . $class );
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 *
		 * @author Juan Coronel
		 *
		 * @since  1.0
		 **/
		public function init_classes() {
			// $this->function = YITH_PMP_Other_Class::get_instance();
			// $this->ajax = YITH_PMP_Ajax::get_instance();
			// $this->compatibility = YITH_PMP_Compatibility::get_instance();
			$this->shortcodes = YITH_PMP_Shortcodes::get_instance();
			YITH_PMP_Post_Types::get_instance();
		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 *
		 * @author Juan Coronel Mendez
		 * @since  1.0
		 * @return void
		 * @access protected
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_PMP_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_PMP_Frontend::get_instance();
			}
		}

	}
}
/**
 * Get the YITH_PMP_Plugin_MyPlugin instance
 *
 * @return YITH_PMP_Plugin_MyPlugin
 */
if ( ! function_exists( 'yith_pmp_plugin_myplugin' ) ) {
	/**
	 * Yith_PMP_plugin_MyPlugin
	 *
	 * @return Object?
	 */
	function yith_pmp_plugin_myplugin() {
		return YITH_PMP_Plugin_MyPlugin::get_instance();
	}
}
