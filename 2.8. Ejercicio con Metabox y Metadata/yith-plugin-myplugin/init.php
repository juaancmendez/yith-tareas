<?php
/**
 * Plugin Name: YITH Plugin My Plugin
 * Description: My Plugin for YITH Plugins
 * Version: 1.0.0
 * Author: Juan Coronel Mendez
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-myplugin
 *
 * @package .
 */

! defined( 'ABSPATH' ) && exit;   // Before all, check if defined ABSPATH.

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'YITH_PMP_VERSION' ) ) {
	define( 'YITH_PMP_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PMP_DIR_URL' ) ) {
	define( 'YITH_PMP_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PMP_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PMP_DIR_ASSETS_URL', YITH_PMP_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PMP_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PMP_DIR_ASSETS_CSS_URL', YITH_PMP_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PMP_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PMP_DIR_ASSETS_JS_URL', YITH_PMP_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PMP_DIR_PATH' ) ) {
	define( 'YITH_PMP_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PMP_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PMP_DIR_INCLUDES_PATH', YITH_PMP_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PMP_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PMP_DIR_TEMPLATES_PATH', YITH_PMP_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PMP_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PMP_DIR_VIEWS_PATH', YITH_PMP_DIR_PATH . 'views' );
}

// Different way to declare a constant.

! defined( 'YITH_PMP_INIT' ) && define( 'YITH_PMP_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_PMP_SLUG' ) && define( 'YITH_PMP_SLUG', 'yith-plugin-myplugin' );
! defined( 'YITH_PMP_SECRETKEY' ) && define( 'YITH_PMP_SECRETKEY', 'zd9egFgFdF1D8Azh2ifA' );
! defined( 'YITH_PMP_OPTIONS_PATH' ) && define( 'YITH_PMP_OPTIONS_PATH', YITH_PMP_DIR_PATH . 'plugin-options' );

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_pmp_init_classes' ) ) {
	/**
	 * Yith_pmp_init_classes
	 *
	 * @return void
	 */
	function yith_pmp_init_classes() {

		load_plugin_textdomain( 'yith-plugin-myplugin', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// Require all the files you include on your plugins. Example.
		require_once YITH_PMP_DIR_INCLUDES_PATH . '/class-yith-pmp-plugin-myplugin.php';

		if ( class_exists( 'YITH_PMP_Plugin_MyPlugin' ) ) {
			/*
			*	Call the main function
			*/
			yith_pmp_plugin_myplugin();
		}
	}
}

add_action( 'plugins_loaded', 'yith_pmp_init_classes', 11 );
