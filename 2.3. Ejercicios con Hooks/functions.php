<?php
/**
 * Twenty Twenty functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

/**
 * Show at the end of each post a typical message: "This post has been written by Christian"
 *
 * @param  string $the_content .
 * @return $the_content
 */
function twentytwenty_exercise_1( $the_content ) {
	$the_content .= '<br><p style=text-align:center;>Este post ha sido escrito por Christian</p>';
	return $the_content;
}

add_filter( 'the_content', 'twentytwenty_exercise_1' );

/**
 * Show the name of the author who made that post
 *
 * @param  mixed $the_content .
 * @return $the_content
 */
function twentytwenty_exercise_2( $the_content ) {
	$the_content .= '<br><p style=text-align:center;>Este post ha sido escrito por ' . get_the_author() . '</p>';
	return $the_content;
}

add_filter( 'the_content', 'twentytwenty_exercise_2' );

/**
 * Having created two posts, it shows the post author exchanged.
 *
 * @param  mixed $the_content .
 * @return $the_content
 */
function twentytwenty_exercise_3( $the_content ) {
	$author = get_the_author_meta( 'id' );
	if ( get_the_title() === 'Good night!' ) {
		$user         = get_user_by( 'id', $author + 1 );
		$the_content .= '<br><p style=text-align:center;>Este post ha sido escrito por ' . $user->user_firstname . '</p>';
	} else {
		if ( get_the_title() === 'Hasta mañana!' ) {
			$user         = get_user_by( 'id', $author - 1 );
			$the_content .= '<br><p style=text-align:center;>Este post ha sido escrito por ' . $user->user_firstname . '</p>';
		}
	}
	return $the_content;
}

add_filter( 'the_content', 'twentytwenty_exercise_3' );

/**
 * When saving a post, save a series of random data such as: “name of the author's father”, “name of the author's mother”, “age.”
 * At the end of everything, print this data at the end of the post.
 *
 * @param  mixed $the_content .
 * @return $the_content
 */
function twentytwenty_exercise_4( $the_content ) {
	$post_id = get_the_id();
	update_post_meta( $post_id, 'dad_name', 'Carlos' );
	update_post_meta( $post_id, 'mom_name', 'Ivana' );
	update_post_meta( $post_id, 'age', random_int( 18, 50 ) );
	$the_content .= '<br><p style=text-align:center;>Nombre del padre del autor: ' . get_post_meta( $post_id, 'dad_name', true ) . '</p>
					 <br><p style=text-align:center;>Nombre de la madre del autor: ' . get_post_meta( $post_id, 'mom_name', true ) . '</p>
					 <br><p style=text-align:center;>Edad: ' . get_post_meta( $post_id, 'age', true ) . '</p>';
	return $the_content;
}

add_filter( 'the_content', 'twentytwenty_exercise_4' );

/**
 * Add in the post the title of who has written it.
 *
 * @param  string $the_title .
 * @return $the_title
 */
function twentytwenty_exercise_5( $the_title ) {
	$the_title .= '<br><p style=text-align:center;font-style:normal;font-size:18px;font-weight:300;> Escrito por ' . get_the_author() . '</p>';
	return $the_title;
}

add_filter( 'the_title', 'twentytwenty_exercise_5' );

/**
 * Add in the same post, the publication date of the post.
 *
 * @param  mixed $the_title .
 */
function twentytwenty_exercise_6( $the_title ) {
	$the_title .= '<br><p style=text-align:center;font-style:normal;font-size:18px;font-weight:300;> Publicado el ' . get_the_date( 'd/m/Y' ) . '</p>';
	return $the_title;
}

add_filter( 'the_title', 'twentytwenty_exercise_6' );

/**
 * From the post, replace the author's name without deleting the custom code that I created previously.
 *
 * @param  mixed $the_content .
 * @return $the_content
 */
function twentytwenty_exercise_7( $the_content ) {
	$nombre      = get_the_author();
	$the_content = str_replace( $nombre, 'changed_name', $the_content );
	return $the_content;
}

add_filter( 'the_content', 'twentytwenty_exercise_7' );
