<?php
/**
 * Plugin Name: Latest 5 Testimonials
 * Plugin URI: http://wordpress.org/extend/plugins/#
 * Description: This is an example plugin
 * Author: Juan Coronel
 * Version: 1.0
 * Author URI: http://example.com/
 *
 * @package .
 */

// register My_Widget.
add_action(
    'widgets_init',
    function() {
        register_widget( 'Latest_5_Testimonials' );
    },
);

/**
 * My_Widget
 */
class Latest_5_Testimonials extends WP_Widget {
    /**
     * __construct.
     *
     * @return void
     */
    public function __construct() {
        $widget_ops = array(
            'classname'   => 'latest_5_testimonials',
            'description' => 'Show the latest 5 testimonials',
        );
        parent::__construct( 'latest_5_testimonials', 'Latest 5 Testimonials', $widget_ops );
    }


    /**
     * Output the widget content on the front-end.
     *
     * @param  mixed $args .
     * @param  mixed $instance .
     * @return void
     */
    public function widget( $args, $instance ) {
        
        $testimonials = get_transient( 'yith_pt_latest_three_testimonials' );

        if ( ! $testimonials ) {

            $query = array(
                'numberposts'    => 3,
                'post_type'      => 'testimonial',
                'publish_status' => 'published',
                'orderby'        => 'date',
                'order'          => 'DESC',
            );

            $posts = get_posts( $query );

            if ( ! empty( $posts ) ) {

                set_transient( 'yith_pt_latest_three_testimonials', $posts, 3 * 3600 );

            };

        }
        echo '<ul> Ultimos 3 Testimonios: ';
        foreach ( $testimonials as $t ) {
            echo '<li>' . esc_html( $t->post_title ) . '</li>';
        }
        echo '</ul>';
    }
    
    
    /**
     * Output the option form field in admin Widgets screen.
     *
     * @param mixed $instance .
     * @return void
     */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Title', 'text_domain' );
        ?>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
                <?php esc_attr_e( 'Title:', 'text_domain' ); ?>
            </label>
    
            <input
                class="widefat"
                id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
                type="text"
                value="<?php echo esc_attr( $title ); ?>">
        <?php
    }
        
    /**
     * Save options.
     *
     * @param  mixed $new_instance .
     * @param  mixed $old_instance .
     * @return string $instance .
     */
    public function update( $new_instance, $old_instance ) {
        $instance                   = array();
        $instance['title']          = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $selected_posts             = ( ! empty( $new_instance['selected_posts'] ) ) ? (array) $new_instance['selected_posts'] : array();
        $instance['selected_posts'] = array_map( 'sanitize_text_field', $selected_posts );

        return $instance;
    }

}
